aiohttp-jinja2 (1.6-1) unstable; urgency=medium

  * New upstream release
    - no longer FTBFS (closes: 1061008)

 -- Piotr Ożarowski <piotr@debian.org>  Tue, 11 Jun 2024 13:01:42 +0200

aiohttp-jinja2 (1.5.1-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.

  [ Piotr Ożarowski ]
  * New upstream release
  * add python3-pytest, python3-pytest-cov, python3-pytest-aiohttp and
    python3-pytest-asyncio to Build-Depends

 -- Piotr Ożarowski <piotr@debian.org>  Wed, 01 Feb 2023 21:34:52 +0100

aiohttp-jinja2 (1.2.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Thu, 16 Sep 2021 23:59:28 -0400

aiohttp-jinja2 (1.2.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed typo in enhances field
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

  [ Piotr Ożarowski ]
  * New upstream release
  * Standards-version bumped to 4.5.0 (no other changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Mon, 16 Mar 2020 12:05:45 +0100

aiohttp-jinja2 (0.8.0-1) unstable; urgency=low

  * Initial release (closes: 832977)

 -- Piotr Ożarowski <piotr@debian.org>  Sat, 30 Jul 2016 15:20:55 +0200
